from food import Food
from scoreboard import Scoreboard
from snake import Snake
from turtle import Screen
import time

s = Screen()
s.setup(width=600, height=600)
s.bgcolor("black")
s.title("WELCOME TO SNAKE GAME")
s.tracer(0)

snake = Snake()
food = Food()
scoreboard = Scoreboard()

s.listen()
s.onkey(snake.up, "Up")
s.onkey(snake.down, "Down")
s.onkey(snake.left, "Left")
s.onkey(snake.right, "Right")

is_game_on = True
while is_game_on:
    s.update()
    time.sleep(0.1)
    snake.move()

    # handle snake eats food
    if snake.snake_head.distance(food) < 15:
        food.spawn_random()
        scoreboard.increase_score()
        snake.grow_snake()

    # handle snake hits wall
    if snake.snake_head.xcor() > 280 or \
            snake.snake_head.xcor() < -280 or \
            snake.snake_head.ycor() > 280 or \
            snake.snake_head.ycor() < -280:
        scoreboard.reset()
        snake.reset()

    # handle snake eats tail
    for seg in snake.segments[1:]:
        if seg == snake.snake_head:
            pass
        if snake.snake_head.distance(seg) < 10:
            scoreboard.reset()
            snake.reset()

s.exitonclick()
