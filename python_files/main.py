# ---  READ  ----
# we need to .close() after .open() to release CPU/RAM resources
# file = open("my_file.txt")
# contents = file.read()
# print(contents)
# file.close()

# we don't have to .close() the file if we use "with"
# with open("my_file.txt") as file:
#     contents = file.read()
#
# print(contents)

# ---  WRITE (override/create) ----
# with open("my_file.txt", mode="w") as file:
#     file.write("Pew pew pew")

# --- APPEND ---
with open("my_file.txt", mode="a") as file:
    file.write("\npew")

