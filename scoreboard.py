from turtle import Turtle


class Scoreboard(Turtle):
    def __init__(self):
        super().__init__()
        self.score = 0
        with open("high_score_data.txt", mode="r") as file_data:
            self.high_score = int(file_data.read())
        self.hideturtle()
        self.color("white")
        self.penup()
        self.setpos(-15.0, 280.0)
        self.update_scoreboard()

    def increase_score(self):
        self.score += 1
        self.update_scoreboard()

    def update_scoreboard(self):
        self.clear()
        self.write(f"SCORE = {self.score} HIGH_SCORE = {self.high_score}", align="center", font=("Arial", 12, "normal"))

    def reset(self):
        if self.score > self.high_score:
            self.high_score = self.score
            with open("high_score_data.txt", mode="w") as file_data:
                file_data.write(str(self.high_score))
        self.score = 0
        self.update_scoreboard()

