from turtle import Turtle

STARTING_POSITIONS = [(0, 0), (-20, 0), (-40, 0)]
MOVE_DISTANCE = 20
UP = 90
DOWN = 270
LEFT = 180
RIGHT = 0


class Snake:
    def __init__(self):
        self.segments = []
        self.spawn_snake()
        self.snake_head = self.segments[0]

    def spawn_snake(self):
        for pos in STARTING_POSITIONS:
            self.add_segment(pos)

    def grow_snake(self):
        self.add_segment(self.segments[-1].pos())

    def add_segment(self, position):
        """Creates a snake segment on given @param position and adds it
        to the segments list"""
        segment = Turtle()
        segment.shape("square")
        segment.color("white")
        segment.penup()
        segment.goto(position)
        self.segments.append(segment)

    def move(self):
        for seg_i in range(len(self.segments) - 1, 0, -1):
            new_x = self.segments[seg_i - 1].xcor()
            new_y = self.segments[seg_i - 1].ycor()
            self.segments[seg_i].goto(new_x, new_y)
        self.snake_head.forward(MOVE_DISTANCE)

    def up(self):
        if self.snake_head.heading() != DOWN:
            self.snake_head.setheading(UP)

    def down(self):
        if self.snake_head.heading() != UP:
            self.snake_head.setheading(DOWN)

    def left(self):
        if self.snake_head.heading() != RIGHT:
            self.snake_head.setheading(LEFT)

    def right(self):
        if self.snake_head.heading() != LEFT:
            self.snake_head.setheading(RIGHT)

    def reset(self):
        for seg in self.segments:
            seg.color("black")
        self.segments.clear()
        self.spawn_snake()
        self.snake_head = self.segments[0]